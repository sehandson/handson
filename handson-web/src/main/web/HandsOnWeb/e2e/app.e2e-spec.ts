import { HandsOnWebPage } from './app.po';

describe('hands-on-web App', () => {
  let page: HandsOnWebPage;

  beforeEach(() => {
    page = new HandsOnWebPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
