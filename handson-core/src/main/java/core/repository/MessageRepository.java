package core.repository;


import core.model.Message;

/**
 * Created by Nicu on 09-Apr-17.
 */
public interface MessageRepository extends GenericRepository<Message, Integer> {

}
