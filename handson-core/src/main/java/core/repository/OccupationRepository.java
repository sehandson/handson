package core.repository;


import core.model.Occupation;

/**
 * Created by Nicu on 09-Apr-17.
 */
public interface OccupationRepository extends GenericRepository<Occupation, Integer> {

}
