package core.repository;


import core.model.Settings;

/**
 * Created by Nicu on 09-Apr-17.
 */
public interface SettingsRepository extends GenericRepository<Settings, Integer> {

}
