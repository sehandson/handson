package core.repository;


import core.model.User;

/**
 * Created by Nicu on 09-Apr-17.
 */
public interface UserRepository extends GenericRepository<User, Integer> {

}
