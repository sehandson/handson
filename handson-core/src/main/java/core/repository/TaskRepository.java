package core.repository;


import core.model.Task;

/**
 * Created by Nicu on 09-Apr-17.
 */
public interface TaskRepository extends GenericRepository<Task, Integer> {

}
