package core.model;

import lombok.*;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Nicu on 01-May-17.
 */
@Entity
@Table(name = "messages")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
public class Message extends BaseEntity<Integer> {
    @ManyToOne(fetch= FetchType.EAGER, optional = false)
    @JoinColumn(name="task")
    private Task task;
    //@ManyToOne(fetch= FetchType.EAGER, optional = false)
    //@JoinColumn(name="handyMan")
    @Column(name="senderid", nullable = false)
    private Integer sender;
    @Column(name="messageBody", nullable = false)
    private String messageBody;
    @Column(name="date", nullable = false)
    private Date date;

    @Override
    public String toString() {
        return "Message{" +
                "task=" + task +
                ", sender=" + sender +
                ", messageBody='" + messageBody + '\'' +
                '}';
    }
}
