package core.model;

import lombok.*;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Nicu on 01-May-17.
 */
@Entity
@Table(name = "tasks")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
public class Task extends BaseEntity<Integer> {
    @ManyToOne(fetch= FetchType.EAGER,optional = false)
    @JoinColumn(name="taskType")
    private Occupation taskType;
    @ManyToOne(fetch= FetchType.EAGER,optional = false)
    @JoinColumn(name="handyMan")
    private User handyMan;
    @ManyToOne(fetch= FetchType.EAGER,optional = false)
    @JoinColumn(name="theuser")
    private User user;
    @Column(name="description", nullable = false)
    private String description;
    @Column(name="rating", nullable = false)
    private Integer rating;
    @Column(name="isFinished", nullable = false)
    private Boolean isFinished;
    @OneToMany(fetch=FetchType.EAGER, cascade = CascadeType.ALL, mappedBy="task")
    private List<Message> messages;

    @Override
    public String toString() {
        return "Task{" +
                "taskType=" + taskType +
                ", handyMan=" + handyMan +
                ", user=" + user +
                ", description='" + description + '\'' +
                ", rating=" + rating +
                ", isFinished=" + isFinished +
                '}';
    }
}
