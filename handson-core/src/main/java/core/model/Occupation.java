package core.model;

import lombok.*;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by Nicu on 01-May-17.
 */
@Entity
@Table(name = "occupations")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
public class Occupation extends BaseEntity<Integer> {
    @OneToMany(fetch=FetchType.EAGER, cascade = CascadeType.ALL, mappedBy="occupation")
    private Set<Settings> settings;
    @OneToMany(fetch=FetchType.EAGER, cascade = CascadeType.ALL, mappedBy="taskType")
    private Set<Task> tasks;
    @Column(name="name", nullable = false)
    private String name;

    @Override
    public String toString() {
        return "Occupation{" +
                "settings=" + settings +
                ", tasks=" + tasks +
                ", name='" + name + '\'' +
                '}';
    }
}
