package core.model;

/**
 * Created by Nicu on 01-May-17.
 */

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "settings")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
public class Settings extends BaseEntity<Integer> {
    @OneToOne(fetch= FetchType.EAGER, optional = false)
    @JoinColumn(name="theuser")
    private User user;
    @ManyToOne(fetch=FetchType.EAGER, optional = false)
    @JoinColumn(name="occupation")
    private Occupation occupation;
    @Column(name="locationRange", nullable = false)
    private Integer locationRange;

    @Override
    public String toString() {
        return "Settings{" +
                "user=" + user +
                ", occupation=" + occupation +
                ", locationRange=" + locationRange +
                '}';
    }
}
