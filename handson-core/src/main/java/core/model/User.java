package core.model;

import lombok.*;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

/**
 * Created by Nicu on 01-May-17.
 */
@Entity
@Table(name = "users")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
public class User extends BaseEntity<Integer> {
    @Column(name = "name", nullable = false)
    private String name;
    @Column(name = "registrationDate", nullable = false)
    private Date registrationDate;
    @Column(name = "eMail", nullable = false)
    private String eMail;
    @OneToOne(fetch= FetchType.EAGER, cascade=CascadeType.ALL, mappedBy = "user")
    private Settings settings;
    @Column(name="address", nullable = false)
    private String address;
    @Column(name="telephoneNumber", nullable = false)
    private String telephoneNumber;
    @OneToMany(fetch=FetchType.EAGER, cascade = CascadeType.ALL, mappedBy="user")
    private Set<Task> tasks;
    @OneToMany(fetch=FetchType.EAGER, cascade = CascadeType.ALL, mappedBy="handyMan")
    private Set<Task> tasksResolved;
    //when getting tasks done by handyman verify in this set if task has handyManId == userId

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", registrationDate=" + registrationDate +
                ", eMail='" + eMail + '\'' +
                ", settings=" + settings +
                ", address='" + address + '\'' +
                ", telephoneNumber='" + telephoneNumber + '\'' +
                ", tasks=" + tasks +
                '}';
    }
}
